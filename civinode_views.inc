<?php

/**
 * CiviNode Views related utilities and hacks
 */
function civinode_views_field_item_table($field) {
  //Grab the default from content.module
  $tables = content_views_field_tables($field);
  //We need to modify in two specific places:
  //we need to change the options, and we need to
  //change the handlers.  We grab refs to both,
  //to make the code easier to read.
  $tab_names = array_keys($tables);
  $field_names = array_keys($tables[$tab_names[0]]['fields']);
  $field_params =& $tables[$tab_names[0]]['fields'][$field_names[0]];
  $type = $field['type'];
  //TODO make this general and not contact specific
  //$field_params['option'] = civinode_views_contact_field_options();
  
  if ($type == 'civicrm_contact') {
    $field_params['option'] = 'string';
    $field_params['help'] = 
      t("Use the 'options' field to customize the way a contact displays. For the handler 'Profiled Contact', you can specify the ID of the CiviCRM Profile you want to use for display.  For 'Contact Field', you can specify the CiviCRM field name you want to use (e.g., 'first_name', 'phone', 'city')");
    $field_params['handler'] =
    array(
    'civinode_view_profiled_contact_view_handler' => t('Profiled Contact'),
    'civinode_view_contact_field_view_handler' => t('Contact Field'),
    'civinode_view_contact_link_view_handler' => t('Contact Field Link'),
    'civinode_view_named_object_view_handler' => t('Object Name'),
    );
  }
  else if ($type == 'civicrm_group') {
    $field_params['handler'] =
    array(
    'civinode_views_group_view_handler' => t('Group Title'),
    'civinode_view_named_object_view_handler' => t('Object Name'),
    );

  }
  else {
    $field_params['handler'] =
    array(
    'civinode_view_named_object_view_handler' => t('Object Name'),
    );

  }
  return $tables;
}



function civinode_views_field_item_table_old($field) {
  $fieldname = $field['field_name'];
  $fieldtype = $field['type'];
  $db_info = content_database_info($field);
  $table_name = $db_info['table'];
  $tables[$fieldname] =
   array(
     'name' => $table_name,
     'provider' => 'civinode_cck',
     'join' =>
        array(
          'left' =>
            array(
              'table' => 'node',
              'field' => 'nid'),
          'right' =>
             array('field' => 'nid')
        ),
        'fields' =>
          array(
            $fieldname =>
              _civinode_views_field_def($field),
          ),
                
   );
   return $tables;
}

function civinode_view_contact_field_view_handler($fieldinfo, $fielddata, $value, $data) {
  if (!is_numeric($value) or $value == 0)
    return FALSE;
  $contact = array();
  _civinode_cck_cache_mgr('fetch', '_crm_views', $value,$contact);
  if (!$contact)
    return FALSE;
  $field_name = $fielddata['options'] ?
                   $fielddata['options']:
                   'display_name';
  $modified = $contact[$field_name];
  if ($modified)
    return $modified;
  else
    return $value;   
}

function civinode_view_contact_link_view_handler($fieldinfo, $fielddata, $value, $data) {
  //We wrap the formatted value into a link
  $text = civinode_view_contact_field_view_handler($fieldinfo, 
                                                  $fielddata, $value, 
                                                  $data);
  $link = l($text, "civicrm/contact/view", array(), 
            "reset=1&cid=$value", NULL, FALSE, TRUE);
  return $link; 
}

function civinode_view_profiled_contact_view_handler($fieldinfo, $fielddata, $value, $data) {
  if (!is_numeric($value) or $value == 0)
    return FALSE;
  $contact = array();
  _civinode_cck_cache_mgr('fetch', '_crm_views', $value,$contact);
  if (!$contact)
    return FALSE;
  $profile = $fielddata['options'];
  if (!$profile) {
    $profile = civinode_get_default_profile_id();
  }
  $profile = $profile and is_numeric($profile) ?
             $profile : 1;
  //$modified = "profiled contact for $value";
  $modified = theme('crm_profile_cid', $profile, $value);
  return $modified;  
}

function civinode_view_named_object_view_handler($fieldinfo, $fielddata, $value, $data) {
  if (!is_numeric($value) or $value == 0)
    return FALSE;
  $modified = "object title for $value";
  return $modified;  
}


function civinode_views_contact_view_handler($fieldinfo, $fielddata, $value, $data){
  if (!is_numeric($value) or $value == 0)
    return FALSE;
  $contact = array();
  _civinode_cck_cache_mgr('fetch', '_crm_views', $value,$contact);
  if (!$contact)
    return FALSE;
  $modified = "crm field data for $value";
  return $modified;
}


function civinode_views_group_view_handler($fieldinfo, $fielddata, $value, $data) {
  if ($value) {
    $group = civinode_get_group_by_id($value);
    if (civinode_check_error($group)) 
      return $value;
    return $group->title;
  }
  return $value;
}


function _civinode_views_field_def($field) {
  $fieldtype = $field['type'];
  $fieldname = $field['field_name'] . "_object_id";
  $label = $field['widget']['label'];
  $t_args = array('%name' => $label);
  $options = NULL;
  $help = t('CiviCRM linked field');
  if ($fieldtype == 'civicrm_contact') {
    $name = t('CiviCRM Contact: %name', $t_args);
    $options = civinode_views_contact_field_options();
    $handler = 'civinode_views_contact_view_handler';
    $help = t('CiviCRM Contact');
  }
  else if ($fieldtype == 'civicrm_group') {
    $name = t('CiviCRM Group: %name', $t_args);
    $handler = 'civinode_views_group_view_handler';
    $help = t('CiviCRM Group');
  }
  else
    $name = t('CiviCRM: %name', $t_args);
  $def = array('name' => $name,
               'help' => $help,
               'field' => $fieldname,
              );
  if ($options)
    $def['option'] = $options;
  if ($handler)
    $def['handler'] = $handler;
    
  return $def;
}


/**
 * Options fields
 *
 */

function civinode_views_contact_field_options() {
  
  $field_options =
    array(
      'display_name' => t('Display Name'),
      'sort_name' => t('Sort Name'),
      'last_name' => t('Last Name'),
      'first_name' => t('First Name'),
      'street_address' => t('Street Address'),
      'city' => t('City'),
      'postal_code' => t('Zip/Postal Code'),
      'other' => t('Other (Enter Below)')
    );
  $form =
    array(
      'main_option' =>
        array(
            '#type' => 'select',
            '#options' => $field_options, 
        ),
        'other_option' =>
        array(
             '#type' => 'textfield',
             '#size' => 20,
        ),
        '#type' => 'civinode_views_option',
        '#process' => array('civinode_views_option_process' => NULL),
        '#after_build' => array('civinode_views_option_builder')
      
    );
    
    return $form;
}


/**
* The #process takes the serialized #default_value and feeds
* the forms beneath it.
*/
function civinode_views_option_process($element) {
  $values = unserialize($element['#default_value']);
  if (!$element) {
    $element = civinode_views_contact_field_options();
  }
  if (!is_array($values)) {
    // set default values for options that have no stored value.
    $values = array('main_option' => 'display_name', 'other_option' => '');
  }
  $element['main_option']['#default_value'] = $values['main_option'];
  $element['other_option']['#default_value'] = $values['other_option'];
  return $element;
}

/**
* Put the value back.
*/
function civinode_views_option_builder($element) {
  global $form_values;

  $op = isset($form_values['op']) ? $form_values['op'] : '';
  
  $values = array();
  $values['main_option'] = $element['main_option']['#value'];
  if ($values['main_option'] != 'other')
    $other = '';
  else 
    $other = $element['other_option']['#value'];
  $values['other_option'] = $other;
  $element['#value'] = serialize($values);
  form_set_value($element, $element['#value']);
  return $element;
}

/**
* this forces the #value to not be printed as it would be if we put in
* no #type.
*/
function theme_civinode_views_option($element) {
  return $element['#children'];
}