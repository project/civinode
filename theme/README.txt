
Custom Theming Of CiviCRM Contacts
==================================

The files in this directory are a simple example of how you
can theme crm contacts using PHPTemplate.  The contents of
theme.php go in your theme's theme.php file;  the very basic
crm_contact.tpl.php file is a sample of formatting a contact
as a table.  Since the default implementation of theme('crm_contact')
already does more than this, you will certainly want to improve it.

Currently, the following variables are delivered to the template:

    'contact' => An associative array of the contact info
    'cck_field' => The CCK $field object
    'field_info' => An array of formatting info taken from the
                    CiviCRM profile object
    'profile_id' => The profile_id of the profile.

This will likely be improved incrementally, but that should get
people started out.


