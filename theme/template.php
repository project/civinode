<?php

//Test civicrm contact formatter
function phptemplate_crm_contact(&$contact, &$field) {
  if (!civinode_check_init())
    return;
  $contact_id = $contact['contact_id'];
  $pid = $field['default_profile_id'] ? 
    $field['default_profile_id'] : 
      civinode_get_default_profile_id(0);
      
  $field_list = civinode_get_profile_field_list($pid, 'view');
  $field_info = array();
  //profiles name some fields using the location id, we want
  //to strip this out:
  foreach ($field_list as $node_field => $raw_field) {
    $field_desc = civinode_get_field_info($pid, $raw_field, 'view');
    $field_info[$node_field] = $field_desc; 
  }
  
  $variables = array(
    'contact' => $contact,
    'cck_field' => $field,
    'field_info' => $field_info,
    'profile_id' => $pid,
    );

  return _phptemplate_callback('crm_contact', $variables, array('crm_contact_'. $pid));
}
//end of test code.
