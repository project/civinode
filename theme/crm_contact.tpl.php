<!-- simple contact formatter -->
<div class="crm-contact crm-contact-<?php print $pid ?>">
  <?php $even = true ?>
  <table>
    <?php foreach ($field_info as $key => $this_field) :
      if (!empty($contact[$key])) : ?>
              <tr class="<?php print($even ? "even" : "odd"); $even = !$even ?>">
                <th>
                <?php print($this_field['title']) ?>
                </th>
                <td>
                <?php print($contact[$key]) ?>
                </td>
              </tr>
   <?php endif;
      endforeach;?>
  </table>
</div>

