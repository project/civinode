CiviCRMData -- Including CiviCRM data in CCK based nodes

Note: this module is deprecated and will be removed from CVS soon.  We recommend
the new CiviNode CCK module, which uses the latest CCK APIs, and is actively
being maintained.


Author:
Mark Fredrickson <mark.m.fredrickson@gmail.com>

Overview: CiviCRMData is a module that defines reference fields for data held in CiviCRM
(http://www.civicrm.org/). It uses the Content Creation Kit
(http://drupal.org/project/cck). At this time, the following CiviCRM types are
supported:

* group
* profile
* contact
* location
* activity
* relationship
* tag

At the moment, these fields are not rendered on node views, as it does not seem
logical to render most of them (e.g. how should the module render a location?).
The primary purpose of this module is to hold data for other modules. As CCK 
becomes more useful module developers, these fields should allow developers to
use CiviCRM data in their applications.

CiviCRMData respects the ACL system defined in CiviNode.

Requires:
* CiviNode (http://drupal.org/node/56661)
* CCK (http://drupal.org/project/cck)
* CiviCRM 1.4 (http://www.civicrm.org/)

Installation:
1. Install the required modules
2. Visit admin/modules and install the CiviCRMData modules

Usage:
1. Create a new content type using CCK
2. Include CiviCRM reference fields
3. Write a module to act on those fields (if you want the node to do something
useful with the CiviCRM data :-)

