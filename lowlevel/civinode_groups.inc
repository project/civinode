<?php
/**
 *
 * Low-level group access routines.  This code is very lightly modified
 * code taken from the CiviCRM 1.4 tree and is subject to its license
 * and copyright.
 *
 *
 */

  /**
	 *   Clone of CRM_Contact_BAO_GroupContact::getContactGroup
	 *
	 *   Difference here is the lack of a permissions check which has been
	 *   horking us for the last two weeks...
	 *
     * function to get the list of groups for contact based on status of membership
     *
     * @param int     $contactId       contact id 
     * @param string  $status          state of membership
     * @param int     $numGroupContact number of groups for a contact that should be shown
     * @param boolean $count           true if we are interested only in the count
     *
     * @return array (reference )|int $values the relevant data object values for the contact or
                                      the total count when $count is true
     *
     * $access public
     */
function CiviNode_LL_getContactGroup( $contactId, $status = null, 
																			$numGroupContact = null, $count = false ) {
	civicrm_initialize(TRUE);
  require_once 'CRM/Contact/DAO/GroupContact.php';
	
        if ( $count ) {
            $select = 'SELECT count(DISTINCT civicrm_group_contact.id)';
        } else {
            $select = 'SELECT 
                    civicrm_group_contact.id as civicrm_group_contact_id, 
                    civicrm_group.title as group_title,
                    civicrm_group.visibility as visibility,
                    civicrm_group_contact.status as status, 
                    civicrm_group.id as group_id,
                    civicrm_subscription_history.date as date,
                    civicrm_subscription_history.method as method';
        }

        $where  = ' WHERE civicrm_group_contact.contact_id = ' 
                . CRM_Utils_Type::escape($contactId, 'Integer') 
                ." AND civicrm_group.is_active = '1' ";
        
        if ( ! empty( $status ) ) {
            $where .= ' AND civicrm_group_contact.status = "' 
                    . CRM_Utils_Type::escape($status, 'String') . '"';
        }
        $tables     = array( 'civicrm_group_contact'        => 1,
                             'civicrm_group'                => 1,
                             'civicrm_subscription_history' => 1 );
        $whereTables = array( );
        //$permission = CRM_Core_Permission::whereClause( CRM_Core_Permission::VIEW, $tables, $whereTables ); 
        //$where .= " AND $permission ";
        
        $from = CRM_Contact_BAO_Query::fromClause( $tables );

        $order = $limit = '';
        if (! $count ) {
            $order = ' ORDER BY civicrm_group.title ';

            if ( $numGroupContact ) {
                $limit = " LIMIT 0, $numGroupContact";
            }
        }

        $sql = $select . $from . $where . $order . $limit;

        if ( $count ) {
            return CRM_Core_DAO::singleValueQuery( $sql ); 
        } else {
            $groupContact =& new CRM_Contact_DAO_GroupContact( );
            $groupContact->query($sql);
            $values = array( );
            while ( $groupContact->fetch() ) {
                $id                            = $groupContact->civicrm_group_contact_id;
                $values[$id]['id']             = $id;
                $values[$id]['group_id']       = $groupContact->group_id;
                $values[$id]['title']          = $groupContact->group_title;
                $values[$id]['visibility']     = $groupContact->visibility;
                switch($groupContact->status) { 
                case 'Added': 
                    $prefix = 'in_'; 
                    break; 
                case 'Removed': 
                    $prefix = 'out_'; 
                    break; 
                default: 
                    $prefix = 'pending_'; 
                } 
                $values[$id][$prefix . 'date']      = $groupContact->date; 
                $values[$id][$prefix . 'method']    = $groupContact->method; 
            }
            return $values;
        }
    }

 ?>